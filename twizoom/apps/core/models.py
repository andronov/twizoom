from django.db import models
import datetime
from django.contrib import admin
from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

# Create your models here.
from embed_video.fields import EmbedVideoField


class Category(models.Model):
    CategoryText = models.CharField(max_length=250, blank=True)

class UserProfile(models.Model):
    user = models.ManyToManyField(User, default=1)
    id_profile = models.CharField(max_length=250)
    name = models.CharField(max_length=250)
    nickname = models.CharField(blank=True, max_length=250)
    surname = models.CharField(blank=True, max_length=250)
    email = models.CharField(blank=True, max_length=250)
    count_fol = models.CharField(blank=True, max_length=250)
    profile_img = models.CharField(blank=True, max_length=1000)
    categors = models.ForeignKey(Category, blank=True)
    #follows = models.ManyToManyField('self', related_name='followed_by', symmetrical=False)

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('name','nickname')

class Tweet(models.Model):
    id_twitt = models.CharField(max_length=250)
    count_ret = models.IntegerField(blank=True)
    count_far = models.IntegerField(blank=True)
    twirank = models.IntegerField(blank=True)
    content = models.CharField(max_length=1000)
    url_img = models.CharField(blank=True, max_length=250)
    video = EmbedVideoField(blank=True)  # same like models.URLField()
    url = models.CharField(blank=True, max_length=250)
    user = models.ForeignKey(UserProfile, blank=True)
    categort = models.ForeignKey(Category, blank=True)
    lang = models.CharField(blank=True, max_length=250)
    creation_date = models.DateTimeField(blank=True)

    def get_user(self):
        return self.user.nickname

class TweetAdmin(admin.ModelAdmin):
    list_display = ['content','lang','get_user']

class Meta(Tweet):
        abstract = True
        ordering = ['creation_date']

