from django.contrib import admin

# Register your models here.
from twizoom.apps.core.models import Tweet, TweetAdmin, UserProfile, UserProfileAdmin

admin.site.register(Tweet,TweetAdmin)
admin.site.register(UserProfile,UserProfileAdmin)
