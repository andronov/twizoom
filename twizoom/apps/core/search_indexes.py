import datetime
from haystack import indexes
from twizoom.apps.core.models import Tweet


class TweetIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True,template_name="search/indexes/core/tweet_text.txt")
    content = indexes.CharField(model_attr='content')
    #creation_date = indexes.DateTimeField(model_attr='creation_date')

    content_auto = indexes.EdgeNgramField(model_attr='content')

    def get_model(self):
        return Tweet

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

