from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns

from django.contrib import admin
#from twizoom.apps.core import forms

admin.autodiscover()

urlpatterns = i18n_patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'twizoom.apps.core.views.full_index'),

    #url(r'^search/', include('haystack.urls')),
    url(r'^sim/(?P<id>\d+)/$', 'twizoom.apps.core.views.similar'),

    url(r'^full/', 'twizoom.apps.core.views.getTweetsy'),
    url(r'^retweet/(?P<cat>\w+)', 'twizoom.apps.core.views.full_retweet_cat'),
    url(r'^retweet', 'twizoom.apps.core.views.full_retweet'),

    url(r'^world/', 'twizoom.apps.core.views.full_world'),

    url(r'^user/', 'twizoom.apps.core.views.AddUser'),

    url(r'^(?P<cat>\w+)', 'twizoom.apps.core.views.full_index_cat'),


   #url(r'^test/(?P<cat>\d{1})/$', 'twizoom.apps.core.views.category'),

)
