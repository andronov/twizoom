"""
Django settings for twizoom project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os


#HAYSTACK_SITECONF='search_indexes'



BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '@r=d*@2y*z^r9=%=wp9m*(tftc9stgqc(8c3r=b82w_a3h06u2'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'localeurl',
    'embed_video',
    'endless_pagination',
    'djcelery',
    'watson',
    'easy_pjax',
    #'elasticsearch',
    'haystack',
    'twizoom.apps.core',
    'south',
    'httplib2',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',

    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

)

ROOT_URLCONF = 'twizoom.urls'

WSGI_APPLICATION = 'twizoom.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'myzoom',
        'USER': 'postgres',
        'PASSWORD': 'rjierb18',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
LOCALEURL_USE_SESSION = True

LANGUAGE_CODE = 'en'

from django.utils.translation import ugettext_lazy as _
LANGUAGES = (
    ('en', _('English')),
    ('ru', _('Русский')),
    ('sv', _('Svenska')),
    ('pl', _('Polski')),
    ('fi', _('Suomi')),
    ('cs', _('Čeština')),
    ('hu', _('Magyar')),
    ('tl', _('Filipino')),
    ('nl', _('Nederlands')),
    ('ur', _('اردو')),
    ('tr', _('Türkçe')),
    ('it', _('Italiano')),
    ('fr', _('Français')),
    ('ko', _('한국어')),
    ('de', _('Deutsch')),
    ('ja', _('日本語')),
    ('pt', _('Português')),
    ('bn', _('বাংলা')),
    ('es', _('Español')),
    ('hi', _('हिन्दी')),
    ('ar', _('العربية')),
)
# указываем, где лежат файлы перевода
LOCALE_PATHS = (
    'locale',
    # os.path.join(PROJECT_DIR, 'locale'),
)




TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

#USE_TZ = True


TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.i18n',

)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
    '/home/andrey/projects/twizoom/static/',
)
STATIC_URL = '/static/'


TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)


HAYSTACK_CONNECTIONS = {
'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': 'haystack',
    },
}

HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'


EMBED_VIDEO_BACKENDS = (
    'embed_video.backends.YoutubeBackend',
    'embed_video.backends.VimeoBackend',
    'embed_video.backends.SoundCloudBackend',
    #'my_app.backends.CustomBackend',
)
